// DISPLAY COMPANY LIST

function getCompanies() {
  const getUrl = 'http://localhost:8080/companies';
  fetch(getUrl)
    .then(response => {
      return response.json();
    })
    .then(function(companies) {
      clearCompaniesListDiv();

      for (let i = 0; i < companies.length; i++) {
        displayCompany(companies[i]);
      }

      hideModal();
    });
}

function getCompany(id) {
  const getUrl = 'http://localhost:8080/companies/company/' + id;
  fetch(getUrl)
    .then(response => response.json())
    .then(company => {
      showModal();
      fillCompanyEditForm(company);
    });
}

function displayCompany(company) {
  getCompaniesListDiv().innerHTML +=
    '<tr>' +
    generateCompanyDataPanel(company) +
    generateCompanyLogo(company) +
    generateCompanyChangeButton(company) +
    generateCompanyDeleteButton(company) +
    '</tr>';
}

function generateCompanyDataPanel(company) {
  return '<td>' + company.id + '</td>' +
    '<td>' + company.name + '</td>';
}

function generateCompanyLogo(company) {
  return '<td><img src="' + company.logo + '"/></td>';
}

function generateCompanyChangeButton(company) {
  return '<td><button type="button" class="btn btn-dark btn-sm" onClick="getCompany(' + company.id + ')">Change</button></td>';
}

function generateCompanyDeleteButton(company) {
  return '<td><button class="btn btn-danger btn-sm" onClick="deleteCompany(' + company.id + ')">X</button></td>';
}

function fillCompanyEditForm(company) {
  getCompanyIdInput().value = company.id;
  getCompanyNameInput().value = company.name;
  getCompanyLogoInput().value = company.logo;
}

function getCompaniesListDiv() {
  return document.getElementById("companyList");
}

function clearCompaniesListDiv() {
  getCompaniesListDiv().innerHTML = "";
}

// ADD AND CHANGE COMPANY

function hideModal() {
  $('#companyEditModal').modal('hide');
}

function showModal() {
  clearCompaniesErrorPanel();
  clearCompanyIdInput();
  clearCompanyNameInput();
  clearCompanyLogoInput();
  $('#companyEditModal').modal('show');
}

function addOrChangeCompany(edit) {
  event.preventDefault();

  if (isCompanyFormValid()) {
    if (getCompanyIdInput().value > 0) {
      changeCompany();
    } else {
      addCompany();
    }
  }
}

function addCompany() {
  const addUrl = 'http://localhost:8080/companies/company';
  fetch(addUrl, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        'name': getCompanyNameInput().value,
        'logo': getCompanyLogoInput().value
      })
    })
    .then(response => getCompanies());
}

function changeCompany() {
  const changeUrl = 'http://localhost:8080/companies/company';
  fetch(changeUrl, {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        'id': getCompanyIdInput().value,
        'name': getCompanyNameInput().value,
        'logo': getCompanyLogoInput().value
      })
    })
    .then(response => getCompanies());
}

function isCompanyFormValid() {
  if (getCompanyNameInput().value == null || getCompanyNameInput().value.length < 1) {
    showCompanyFormError("Ettevõtte nimi puudub!")
    return false;
  } else if (getCompanyLogoInput().value == null || getCompanyLogoInput().value.length < 1) {
    showCompanyFormError("Ettevõtte logo puudub!")
    return false;
  }
  return true;
}

function uploadFile(event) {
  event.preventDefault();

  let formData = new FormData();
  formData.append("file", getCompanyFileInput().files[0]);

  const fileUploadUrl = 'http://localhost:8080/files/upload';
  fetch(fileUploadUrl, {
    method: 'POST',
    body: formData
  }).then(
    response => response.json()
  ).then(
    success => {
      console.log('File uploaded', success);
      getCompanyLogoInput().value = success.url;
    }
  ).catch(
    error => console.log(error)
  );
}

function getCompanyIdInput() {
  return document.getElementById('id');
}

function getCompanyNameInput() {
  return document.getElementById('name');
}

function getCompanyLogoInput() {
  return document.getElementById('logo');
}

function getCompanyFileInput() {
  return document.getElementById('file');
}

function clearCompanyIdInput() {
  getCompanyIdInput().value = null;
}

function clearCompanyNameInput() {
  getCompanyNameInput().value = null;
}

function clearCompanyLogoInput() {
  getCompanyLogoInput().value = null;
}

function getCompaniesErrorPanel() {
  return document.getElementById('errorPanel');
}

function showCompanyFormError(errorText) {
  getCompaniesErrorPanel().innerHTML = errorText;
  getCompaniesErrorPanel().style.display = "block";
}

function clearCompaniesErrorPanel() {
  getCompaniesErrorPanel().innerHTML = "";
  getCompaniesErrorPanel().style.display = "none";
}

// DELETE COMPANY

function deleteCompany(id) {
  if (confirm('Oled kindel, et soovid ettevõtet kustutada?')) {
    const deleteUrl = 'http://localhost:8080/companies/company/' + id;
    fetch(deleteUrl, {
        method: "DELETE"
      })
      .then(response => getCompanies());
  }
}